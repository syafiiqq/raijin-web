<?php
  require 'vendor/autoload.php';
  use \Mailjet\Resources;

  if (isset($_POST['action'])) {
    sendEmail();
  }

  function sendEmail() {
    $name = $_POST['name'];
    $email = $_POST['mail'];
    $request = $_POST['requestOp'];
    $message = $_POST['msg'];
    
    $mj = new \Mailjet\Client('0be15c2ddbc659be9dbfe8dec50b25ea','7e79e78f81787a6ebb88820dd40d85b4',true,['version' => 'v3.1']);
    $body = [
      'Messages' => [
        [
          'From' => [
            'Email' => "contact@raijintechnologies.com",
            'Name' => "Raijin Web: $name($email) send you e-mail"
          ], 
          'To' => [
            [
              'Email' => "maretashv@gmail.com",
              'Name' => "Contact"
            ]
          ],
          'Subject' => "$request",
          'TextPart' => "Mailjet email",
          'HTMLPart' => "$message",
          'CustomID' => "AppGettingStartedTest"
        ]
      ]
    ];
    $response = $mj->post(Resources::$Email, ['body' => $body]);
    $response->success() && var_dump($response->getData());
  }
?>