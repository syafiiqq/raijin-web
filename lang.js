var arrLang = {
    en: {
      //header
      home: "HOME",
      services: "SERVICES",
      aboutus: "ABOUT US",
      partnerships: "PARTNERSHIPS",
      contact: "CONTACT",
      blog: "BLOG",
      //
      //page1
      random:
        "This is just some of random text that I want to translate to another language",
      one: "one",
      two: "two",
      //
    },
    fr: {
      //status bar
      home: "HOMEFR",
      services: "SERVICESFR",
      aboutus: "ABOUT USFR",
      partnerships: "PARTNERSHIPSFR",
      contact: "CONTACTFR",
      blog: "BLOGfr",
      //
      //page1
      random:
        "Ceci est juste un texte aléatoire que je souhaite traduire dans une autre langue",
      one: "un",
      two: "deux",
      //
    },
  };

  // Process translation
  $(function () {
    $(".translate").click(function () {
      var lang = $(this).attr("id");
      var current = document.getElementsByClassName("activeLang");
      current[0].className = current[0].className.replace(" activeLang", "");
      this.className += " activeLang";

      $(".lang").each(function (index, item) {
        $(this).text(arrLang[lang][$(this).attr("key")]);
      });
    });
  });



  function  EnFunction() {
    document.getElementById("typedFr").style.display = "none";
    document.getElementById("typedEn").style.display = "block";
  }
  function  FrFunction() {
    document.getElementById("typedEn").style.display = "none";
    document.getElementById("typedFr").style.display = "block";
  }